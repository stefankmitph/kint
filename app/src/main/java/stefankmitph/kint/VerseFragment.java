package stefankmitph.kint;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import stefankmitph.model.DatabaseManager;
import stefankmitph.model.Word;

public class VerseFragment extends Fragment {

    private String book;
    private int chapter;
    private int verse;
    private List<Word> words;
    private Typeface typeface;
    private ActivityObjectProvider provider;
    private View contentView;
    private String m_title;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public static VerseFragment newInstance(String book, int chapter, int verse) {
        VerseFragment fragment = new VerseFragment();
        fragment.setRetainInstance(true);
        Bundle args = new Bundle();

        args.putString("book", book);
        args.putInt("chapter", chapter);
        args.putInt("verse", verse);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try
        {
            provider = (ActivityObjectProvider) activity;
            //((MainActivity)activity).setActionBarTitle(m_title);

        } catch(ClassCastException e) {
            throw new RuntimeException("it ain't a Provider");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        book = getArguments().getString("book");
        chapter = getArguments().getInt("chapter");
        verse = getArguments().getInt("verse");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser) {
            FragmentActivity activity = getActivity();
            if(activity != null)
            {
                //((MainActivity)activity).setActionBarTitle(String.format("%s %d:%d", book, chapter, verse));
                ((MainActivity)activity).setActionBarTitle(m_title);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Context context = container.getContext();
        contentView = inflater.inflate(R.layout.activity_fragment, null);

        typeface = provider.getTypeface();
        words = provider.getWords(verse);
        int chapter = words.get(0).getChapter();
        int verse = words.get(0).getVerse();
        String book = words.get(0).getBook();

        m_title = book + " " + chapter + " : " + verse;

        FlowLayout flowLayout = (FlowLayout)contentView.findViewById(R.id.flow_layout);

        flowLayout.removeAllViews();

        float textSizeMultiplier = 1;
        Bundle prefs = provider.getPreferences();
        String textSize = prefs.getString("text_size");
        if(textSize != null) {
            switch (textSize.toLowerCase()) {
                case "small":
                    textSizeMultiplier = 0.8f;
                    break;
                case "medium":
                    textSizeMultiplier = 1f;
                    break;
                case "large":
                    textSizeMultiplier = 1.2f;
                    break;
                case "x-large":
                    textSizeMultiplier = 1.4f;
                    break;
            }
        }

        int count = words.size();
        for (int i = 0; i < count; i++) {

            final View verseLayout = inflater.inflate(R.layout.layout_verse_word, null);

            final TextView textViewWord = (TextView)verseLayout.findViewById(R.id.textView_word);
            textViewWord.setText(words.get(i).getWord());
            textViewWord.setTypeface(typeface);
            textViewWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.dimen_word) * textSizeMultiplier);


            final TextView textViewStrongs = (TextView)verseLayout.findViewById(R.id.textView_strongs);
            textViewStrongs.setText(words.get(i).getLemma());
            textViewStrongs.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.dimen_strongs) * textSizeMultiplier);
            textViewStrongs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String text = textViewStrongs.getText().toString();

                    DatabaseManager manager = DatabaseManager.getInstance();
                    String strongs = manager.getStrongsGr(text);

                    new AlertDialog.Builder(getActivity())
                            .setTitle(text + " - " + textViewWord.getText())
                            .setMessage(strongs)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();
                }
            });


            final TextView textViewFunctional = (TextView)verseLayout.findViewById(R.id.textView_functional);
            textViewFunctional.setText(words.get(i).getStrongs());
            textViewFunctional.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.dimen_functional) * textSizeMultiplier);
            textViewFunctional.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    TextView textView = (TextView) v;
                    String entry = (String) textView.getText();
                    DatabaseManager manager = DatabaseManager.getInstance();
                    String result = manager.getGrammarEntry(entry);

                    Toast toast = Toast.makeText(v.getContext(), result, Toast.LENGTH_LONG);
                    toast.show();
                }
            });

            final TextView textViewConcordance = (TextView)verseLayout.findViewById(R.id.textView_concordance);
            textViewConcordance.setText(words.get(i).getConcordance());
            textViewConcordance.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.dimen_concordance) * textSizeMultiplier);

            final TextView textViewTranslit = (TextView)verseLayout.findViewById(R.id.textView_transliteration);
            textViewTranslit.setText(words.get(i).getTranslit());
            textViewTranslit.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.dimen_transliteration) * textSizeMultiplier);

            boolean showStrongs = prefs.getBoolean("show_strongs", false);
            if(!showStrongs)
                textViewStrongs.setVisibility(View.GONE);

            boolean showConcordance = prefs.getBoolean("show_concordance", false);
            if(!showConcordance)
                textViewConcordance.setVisibility(View.GONE);

            boolean showTransliteration = prefs.getBoolean("show_transliteration", false);
            if(!showTransliteration)
                textViewTranslit.setVisibility(View.GONE);

            boolean showFunctional = prefs.getBoolean("show_functional", false);
            if(!showFunctional)
                textViewFunctional.setVisibility(View.GONE);

            flowLayout.addView(verseLayout);
        }

        /*FlowLayout layout = new FlowLayout(context, null);
        layout.setLayoutParams(
                new FlowLayout.LayoutParams(
                        FlowLayout.LayoutParams.MATCH_PARENT,
                        FlowLayout.LayoutParams.WRAP_CONTENT
                ));
        layout.setPadding(10, 10, 10, 10);
        */

        /*if(words == null) {
        }
        else {
            int count = words.size();
            for (int i = 0; i < count; i++) {

                LinearLayout linearLayout = getLayout(context, i);

                layout.addView(linearLayout);
            }
            ((ViewGroup) contentView).addView(layout);
        }*/
        return contentView; // super.onCreateView(inflater, container, savedInstanceState);
    }

    private LinearLayout getLayout(Context context, int index) {

        Bundle prefs = provider.getPreferences();

        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setPadding(10, 20, 30, 40);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));

        final TextView textViewWord = new TextView(context);
        textViewWord.setTextAppearance(context, android.R.style.TextAppearance_Large);
        textViewWord.setTypeface(typeface);
        textViewWord.setTextColor(Color.rgb(61, 76, 83));
        textViewWord.setTag("textViewWord" + index);
        /*textViewWord.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                TextView textView = (TextView) v;
                textView.setBackgroundColor(Color.LTGRAY);

                Menu menu = provider.getMenu();
                getActivity().getMenuInflater().inflate(R.menu.menu_selected_word, menu);

                return true;
            }
        });*/

        final TextView textViewStrongs = new TextView(context);
        textViewStrongs.setTag("textViewStrongs" + index);
        textViewStrongs.setTextAppearance(context, android.R.style.TextAppearance_Small);
        textViewStrongs.setTextColor(Color.rgb(77, 179, 179));
        textViewStrongs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = textViewStrongs.getText().toString();

                DatabaseManager manager = DatabaseManager.getInstance();
                String strongs = manager.getStrongsGr(text);

                //Toast toast = Toast.makeText(v.getContext(), strongs, Toast.LENGTH_LONG);
                //toast.show();

                new AlertDialog.Builder(getActivity())
                        .setTitle(text + " - " + textViewWord.getText())
                        .setMessage(strongs)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        })
                        .show();
            }
        });

        TextView textViewConcordance = new TextView(context);
        textViewConcordance.setTextAppearance(context, android.R.style.TextAppearance_Holo_Small);
        textViewConcordance.setTextColor(Color.rgb(230, 74, 69));
        textViewConcordance.setTag("textViewConcordance" + index);

        TextView textViewTranslit = new TextView(context);
        textViewConcordance.setTextAppearance(context, android.R.style.TextAppearance_Holo_Small);
        textViewConcordance.setTextColor(Color.rgb(230, 74, 69));
        textViewConcordance.setTag("textViewTranslit" + index);

        TextView textViewFunctional = new TextView(context);
        textViewFunctional.setTextAppearance(context, android.R.style.TextAppearance_Small);
        textViewFunctional.setTextColor(Color.rgb(77, 179, 179));
        textViewFunctional.setTag("textViewFunctional" + index);
        textViewFunctional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView textView = (TextView) v;
                String entry = (String) textView.getText();
                DatabaseManager manager = DatabaseManager.getInstance();
                String result = manager.getGrammarEntry(entry);

                Toast toast = Toast.makeText(v.getContext(), result, Toast.LENGTH_LONG);
                toast.show();
            }
        });

        textViewStrongs.setText(words.get(index).getLemma());
        textViewWord.setText(words.get(index).getWord());
        textViewFunctional.setText(words.get(index).getStrongs());
        textViewConcordance.setText(words.get(index).getConcordance());
        textViewTranslit.setText(words.get(index).getTranslit());

        boolean showStrongs = prefs.getBoolean("show_strongs", false);
        if(showStrongs)
            linearLayout.addView(textViewStrongs);

        linearLayout.addView(textViewWord);

        boolean showConcordance = prefs.getBoolean("show_concordance", false);
        if(showConcordance)
            linearLayout.addView(textViewConcordance);


        boolean showTransliteration = prefs.getBoolean("show_transliteration", false);
        if(showTransliteration)
            linearLayout.addView(textViewTranslit);

        boolean showFunctional = prefs.getBoolean("show_functional", false);
        if(showFunctional)
            linearLayout.addView(textViewFunctional);

        return linearLayout;
    }

}
