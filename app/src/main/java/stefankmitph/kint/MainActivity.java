package stefankmitph.kint;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import stefankmitph.model.DatabaseManager;
import stefankmitph.model.Word;

public class MainActivity extends ActionBarActivity implements ActivityObjectProvider {

    private SQLiteDatabase database;
    private Bundle m_bundle;
    private String book;
    private int chapter;
    private int verse;
    private HashMap<Integer, List<Word>> map;
    private Typeface typeface;
    MyPagerAdapter myPagerAdapter;
    private HashMap<Integer, String> m_booksDictionary;
    private Integer m_bookIndex;
    private Menu m_menu;
    public final static int MENU_DEFAULT = 0;
    public final static int MENU_WORD_SELECTED = 10;
    private int m_menuId = MENU_DEFAULT;

    private final HashMap<String, HashMap<Integer, List<Word>>> m_cache = new HashMap<>();

    private static final int RESULT_SETTINGS = 1;

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    private void initializeData(List<Word> words) {
        /*map = new HashMap<>();
        for(Word word : words) {
            if(!map.containsKey(word.getVerse()))
                map.put(word.getVerse(), new ArrayList<Word>());

            map.get(word.getVerse()).add(word);
        }*/
    }

    private void addToCache(String book, Integer chapter, List<Word> words) {
        if(m_cache.containsKey(book)) {
            HashMap<Integer, List<Word>> chapterMap = m_cache.get(book);
            if(!chapterMap.containsKey(chapter)) {
                chapterMap.put(chapter, words);
            }
        } else {
            m_cache.put(book, new HashMap<Integer, List<Word>>());
            HashMap<Integer, List<Word>> newHashMap = m_cache.get(book);
            newHashMap.put(chapter, words);
        }
    }

    private List<Word> getFromCache(String book, Integer chapter) {
        if(m_cache.containsKey(book)) {
            HashMap<Integer, List<Word>> map = m_cache.get(book);
            if(map.containsKey(chapter)) {
                return map.get(chapter);
            }
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        typeface = Typeface.createFromAsset(this.getAssets(), "fonts/Cardo104s.ttf");

        DatabaseManager.init(this);

        readPreferences();

        m_bundle = getIntent().getExtras();
        m_bookIndex = m_bundle.getInt("book_index");
        book = m_bundle.getString("book");
        chapter = m_bundle.getInt("chapter");
        verse = m_bundle.getInt("verse");

        DatabaseManager manager = DatabaseManager.getInstance();
        List<Word> words = manager.getChapter(book, chapter);
        initializeData(words);

        addToCache(book, chapter, words);

        m_booksDictionary = manager.getBooksDictionary();

        setActionBarTitle(String.format("%s %d : %d", book, this.chapter, verse));

        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

        final ViewPager myPager = (ViewPager) findViewById(R.id.home_panels_pager);
        myPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
        myPager.setOffscreenPageLimit(5);
        myPager.setAdapter(myPagerAdapter);
        myPager.setCurrentItem(verse - 1);
        myPager.setOnPageChangeListener(new CircularViewPagerHandler(myPager));
    }

    private void readPreferences() {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);

        boolean keep_screen_on = sharedPreferences.getBoolean("keep_screen_on", true);

        if(keep_screen_on) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        String text_size = sharedPreferences.getString("text_size", "Medium");
        switch (text_size) {

        }
    }

    @Override
    public SQLiteDatabase getDatabase() {
        return this.database;
    }

    @Override
    public List<Word> getWords(final int verse) {
        if(m_cache.containsKey(book)) {
            HashMap<Integer, List<Word>> chapterMap = m_cache.get(book);
            if(chapterMap.containsKey(chapter)) {

                List<Word> words = chapterMap.get(chapter);
                if(words != null) {
                    Collection<Word> filter = Collections2.filter(words, new Predicate<Word>() {
                        @Override
                        public boolean apply(Word input) {
                            return input.getVerse() == verse;
                        }
                    });

                    return Lists.newArrayList(filter);
                }
            }
        }
        return null;
    }

    @Override
    public Typeface getTypeface() {
        return typeface;
    }

    @Override
    public Bundle getPreferences() {

        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);

        Bundle bundle = new Bundle();
        bundle.putBoolean("show_strongs", sharedPrefs.getBoolean("show_strongs", true));
        bundle.putBoolean("show_concordance", sharedPrefs.getBoolean("show_concordance", true));
        bundle.putBoolean("show_functional", sharedPrefs.getBoolean("show_functional", true));
        bundle.putBoolean("show_transliteration", sharedPrefs.getBoolean("show_transliteration", true));
        bundle.putString("text_size", sharedPrefs.getString("text_size", "Medium"));
        return bundle;
    }

    @Override
    public Menu getMenu() {
        return m_menu;
    }

    @Override
    public Context getContext() {
        return getContext();
    }

    @Override
    public void invalidateMenu(int menuId) {
        invalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        switch(m_menuId) {
            case MENU_DEFAULT:
                //getMenuInflater().inflate(R.menu.menu_main, menu);
                break;
            case MENU_WORD_SELECTED:
                //getMenuInflater().inflate(R.menu.menu_selected_word, menu);
                break;
        }
        return true;
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return VerseFragment.newInstance(book, chapter, position + 1);
        }

        @Override
        public int getCount() {
            List<Word> words = m_cache.get(book).get(chapter);

            Multimap<Integer, Word> index = Multimaps.index(words, new Function<Word, Integer>() {
                @Override
                public Integer apply(Word input) {
                    return input.getVerse();
                }
            });

            return index.asMap().size();
        }

        private String getFragmentName(int viewPagerId, int index) {
            return "android:switcher:" + viewPagerId + ":" + index;
        }
    }

    public class CircularViewPagerHandler implements ViewPager.OnPageChangeListener {
        private ViewPager   mViewPager;
        private int m_currentPosition;
        private int         mScrollState;

        public CircularViewPagerHandler(final ViewPager viewPager) {
            mViewPager = viewPager;
        }

        @Override
        public void onPageSelected(final int position) {
            m_currentPosition = position;
        }

        @Override
        public void onPageScrollStateChanged(final int state) {
            handleScrollState(state);
            mScrollState = state;
        }

        private void handleScrollState(final int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
                setNextItemIfNeeded();
            }
        }

        private void setNextItemIfNeeded() {
            if (!isScrollStateSettling()) {
                handleSetNextItem();
            }
        }

        private boolean isScrollStateSettling() {
            return mScrollState == ViewPager.SCROLL_STATE_SETTLING;
        }

        private void handleSetNextItem() {
            final int lastPosition = mViewPager.getAdapter().getCount() - 1;
            final DatabaseManager manager = DatabaseManager.getInstance();

            if(m_currentPosition == 0) {
                if(chapter - 1 == 0) {
                    if(m_booksDictionary.containsKey(m_bookIndex - 1)) {
                        m_bookIndex--;

                        String previousBook = m_booksDictionary.get(m_bookIndex);

                        book = previousBook;
                        chapter = (int)manager.getCountOfChapters(previousBook);
                        verse = (int)manager.getCountOfVerses(previousBook, chapter);
                        m_currentPosition = verse - 1;
                    } else {
                        Integer lastBookIndex = Ordering.natural().max(m_booksDictionary.keySet());
                        String lastBook = m_booksDictionary.get(lastBookIndex);
                        int countOfChapters = (int)manager.getCountOfChapters(lastBook);
                        int countOfVerses = (int)manager.getCountOfVerses(lastBook, countOfChapters);

                        m_bookIndex = lastBookIndex;

                        book = lastBook;
                        chapter = countOfChapters;
                        verse = countOfVerses;
                        m_currentPosition = verse - 1;
                    }
                } else {
                    chapter--;
                    int countOfVerses = (int)manager.getCountOfVerses(book, chapter);
                    verse = countOfVerses; //WordsHelper.getVerseCountOfChapter(getFromCache(book, chapter), chapter);
                    m_currentPosition = verse - 1;
                }
            } else if(m_currentPosition == lastPosition) {

                long countOfChapters = manager.getCountOfChapters(book);

                if(chapter + 1 > countOfChapters) {
                    if(m_booksDictionary.containsKey(m_bookIndex + 1)) {
                        m_bookIndex++;

                        String nextBook = m_booksDictionary.get(m_bookIndex);

                        book = nextBook;
                        chapter = 1;
                        verse = 1;

                    } else {
                        m_bookIndex = 1;
                        book = "Matthew";
                        chapter = 1;
                        verse = 1;
                    }
                } else {
                    //m_bookIndex++;
                    chapter++;
                    verse = 1;
                }
            }

            List<Word> cachedData = getFromCache(book, chapter);
            if(cachedData != null) {
                initializeData(cachedData);
            } else {
                List<Word> words = manager.getChapter(book, chapter);
                addToCache(book, chapter, words);
                initializeData(words);
            }

            myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

            final ViewPager myPager = (ViewPager) findViewById(R.id.home_panels_pager);
            myPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
            myPager.setOffscreenPageLimit(5);
            myPager.setAdapter(myPagerAdapter);
            myPager.setCurrentItem(verse - 1);
            myPager.setOnPageChangeListener(new CircularViewPagerHandler(myPager));

            mViewPager.setCurrentItem(verse - 1, false);

            setActionBarTitle(String.format("%s %d : %d", book, chapter, verse));
        }

        @Override
        public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
            m_currentPosition = position;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        m_menu = menu;

        android.support.v7.app.ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeButtonEnabled(false); // disable the button
            supportActionBar.setDisplayHomeAsUpEnabled(false); // remove the left caret
            supportActionBar.setDisplayShowHomeEnabled(false); // remove the icon
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, RESULT_SETTINGS);
            return true;
        } else if(id == R.id.action_next_chapter) {
            DatabaseManager manager = DatabaseManager.getInstance();
            int countOfChapters = (int)manager.getCountOfChapters(book);
            if(chapter + 1 > countOfChapters) {
                chapter = 1;
                verse = 1;

                if(m_booksDictionary.containsKey(m_bookIndex + 1))
                {
                    book = m_booksDictionary.get(++m_bookIndex);
                } else {
                    book = "Matthew";
                    m_bookIndex = Ordering.natural().min(m_booksDictionary.keySet());
                }

            } else {
                chapter++;
                verse = 1;
            }

            List<Word> cachedData = getFromCache(book, chapter);
            if(cachedData != null) {
                initializeData(cachedData);
            } else {
                List<Word> words = manager.getChapter(book, chapter);
                addToCache(book, chapter, words);
                initializeData(words);
            }

            myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

            final ViewPager myPager = (ViewPager) findViewById(R.id.home_panels_pager);
            myPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
            myPager.setOffscreenPageLimit(5);
            myPager.setAdapter(myPagerAdapter);
            myPager.setCurrentItem(verse - 1);
            myPager.setOnPageChangeListener(new CircularViewPagerHandler(myPager));

            setActionBarTitle(String.format("%s %d : %d", book, chapter, verse));
        } else if(id == R.id.action_previous_chapter) {
            DatabaseManager manager = DatabaseManager.getInstance();
            if(chapter - 1 == 0) {
                verse = 1;

                if(m_booksDictionary.containsKey(m_bookIndex - 1)) {
                    book = m_booksDictionary.get(--m_bookIndex);
                } else {
                    Integer lastBookIndex = Ordering.natural().max(m_booksDictionary.keySet());
                    m_bookIndex = lastBookIndex;
                    book = m_booksDictionary.get(lastBookIndex);
                }

                chapter = (int)manager.getCountOfChapters(book);
            } else {
                chapter--;
                verse = 1;
            }

            List<Word> cachedData = getFromCache(book, chapter);
            if(cachedData != null) {
                initializeData(cachedData);
            } else {
                List<Word> words = manager.getChapter(book, chapter);
                addToCache(book, chapter, words);
                initializeData(words);
            }

            myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

            final ViewPager myPager = (ViewPager) findViewById(R.id.home_panels_pager);
            myPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
            myPager.setOffscreenPageLimit(5);
            myPager.setAdapter(myPagerAdapter);
            myPager.setCurrentItem(verse - 1);
            myPager.setOnPageChangeListener(new CircularViewPagerHandler(myPager));

            setActionBarTitle(String.format("%s %d : %d", book, chapter, verse));
        } else if(id == R.id.action_next_book) {
            DatabaseManager manager = DatabaseManager.getInstance();
            verse = 1;
            chapter = 1;

            if(m_booksDictionary.containsKey(m_bookIndex + 1)) {
                book = m_booksDictionary.get(++m_bookIndex);
            } else {
                m_bookIndex = Ordering.natural().min(m_booksDictionary.keySet());
                book = m_booksDictionary.get(m_bookIndex);
            }

            List<Word> cachedData = getFromCache(book, chapter);
            if(cachedData != null) {
                initializeData(cachedData);
            } else {
                List<Word> words = manager.getChapter(book, chapter);
                addToCache(book, chapter, words);
                initializeData(words);
            }

            myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

            final ViewPager myPager = (ViewPager) findViewById(R.id.home_panels_pager);
            myPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
            myPager.setOffscreenPageLimit(5);
            myPager.setAdapter(myPagerAdapter);
            myPager.setCurrentItem(verse - 1);
            myPager.setOnPageChangeListener(new CircularViewPagerHandler(myPager));

            setActionBarTitle(String.format("%s %d : %d", book, chapter, verse));
        } else if(id == R.id.action_previous_book) {
            DatabaseManager manager = DatabaseManager.getInstance();
            verse = 1;
            chapter = 1;

            if(m_booksDictionary.containsKey(m_bookIndex - 1)) {
                book = m_booksDictionary.get(--m_bookIndex);
            } else {
                m_bookIndex = Ordering.natural().max(m_booksDictionary.keySet());
                book = m_booksDictionary.get(m_bookIndex);
            }

            List<Word> cachedData = getFromCache(book, chapter);
            if(cachedData != null) {
                initializeData(cachedData);
            } else {
                List<Word> words = manager.getChapter(book, chapter);
                addToCache(book, chapter, words);
                initializeData(words);
            }

            myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

            final ViewPager myPager = (ViewPager) findViewById(R.id.home_panels_pager);
            myPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
            myPager.setOffscreenPageLimit(5);
            myPager.setAdapter(myPagerAdapter);
            myPager.setCurrentItem(verse - 1);
            myPager.setOnPageChangeListener(new CircularViewPagerHandler(myPager));

            setActionBarTitle(String.format("%s %d : %d", book, chapter, verse));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SETTINGS:

                readPreferences();
                reloadFragment();
                break;

        }
    }

    private void reloadFragment() {
        ViewPager pager = (ViewPager) findViewById(R.id.home_panels_pager);
        String fragmentId = myPagerAdapter.getFragmentName(R.id.home_panels_pager, pager.getCurrentItem());

        int currentItem = pager.getCurrentItem();
        pager.setAdapter(myPagerAdapter);
        pager.setCurrentItem(currentItem);
    }
}
