package stefankmitph.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class StrongsGr {

    @DatabaseField(unique = true)
    private String key;

    @DatabaseField
    private String lemma;

    @DatabaseField
    private String pronunciation;

    @DatabaseField
    private String definition;

    public String getKey() {
        return key;
    }

    public String getLemma() {
        return lemma;
    }

    public String getPronunciation() {
        return pronunciation;
    }

    public String getDefinition() {
        return definition;
    }
}
