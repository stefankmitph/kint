package stefankmitph.model;

import com.google.common.base.Function;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import java.util.List;

/**
 * Created by KumpitschS on 20.10.2015.
 */
public class WordsHelper {
    public static int getVerseCountOfChapter(List<Word> words, int chapter) {
        Multimap<Integer, Word> index = Multimaps.index(words, new Function<Word, Integer>() {
            @Override
            public Integer apply(Word input) {
                return input.getVerse();
            }
        });

        return index.asMap().size();
    }
}
