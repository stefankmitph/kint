package stefankmitph.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "grammar")
public class Grammar {
    @DatabaseField
    private String key;

    @DatabaseField
    private String value;

    public Grammar() {}

    public String getValue() {
        return value;
    }
}
