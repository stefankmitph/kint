package stefankmitph.model;

import android.content.Context;

import com.j256.ormlite.stmt.PreparedQuery;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import stefankmitph.kint.DataBaseHelper;

/**
 * Created by stefankmitph on 19.04.2015.
 */
public class DatabaseManager {
    static private DatabaseManager instance;

    static public void init(Context context) {
        if(instance == null){
            instance = new DatabaseManager(context);
        }
    }

    static public DatabaseManager getInstance() {
        return instance;
    }

    private DataBaseHelper helper;
    private DatabaseManager(Context context) {
        helper = new DataBaseHelper(context);
        try {
            helper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private DataBaseHelper getHelper() {
        return helper;
    }

    public String getGrammarEntry(String entry) {
        try {
            List<Grammar> grammar = getHelper().getGrammarDao().queryBuilder().where().eq("key", entry).query();
            if(grammar.size() > 0)
                return grammar.get(0).getValue();
        } catch(SQLException ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public HashMap<Integer, String> getBooksDictionary() {
        HashMap<Integer, String> map = new HashMap<>();
        try {
            List<Book> result = getHelper().getBookDao().queryBuilder().orderBy("nr", true).query();
            for(Book book : result) {
                map.put(book.getNr(), book.getName());
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return map;
    }

    public ArrayList<String> getBooks() {
        ArrayList<String> list = new ArrayList<>();
        try {
            List<Book> result = getHelper().getBookDao().queryBuilder().orderBy("nr", true).query();

            for(Book book : result) {
                list.add(book.getName());
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public long getCountOfChapters(String book_name) {
        long count = 0;
        try {
            List<Word> list = getHelper().getWordDao().queryBuilder()
                    .groupByRaw("book, chapter")
                    .where().eq("book", book_name)
                    .query();

            count = list.size();

        } catch(SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public long getCountOfVerses(String book, int chapter) {
        long count = 0;
        try {
            List<Word> list = getHelper().getWordDao().queryBuilder()
                    .groupByRaw("book, chapter, verse")
                    .where().eq("book", book).and().eq("chapter", chapter)
                    .query();

            count = list.size();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public List<Word> getChapter(String book, int chapter) {
        List<Word> list = null;
        try {
            PreparedQuery<Word> query = getHelper().getWordDao().queryBuilder()
                    .orderBy("wordnr", true)
                    .where()
                    .eq("book", book)
                    .and()
                    .eq("chapter", chapter)
                    .prepare();

            list = getHelper().getWordDao().query(query);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getStrongsGr(String nr) {
        try {
            List<StrongsGr> item = getHelper().getStrongsGrDao().queryBuilder()
                    .where()
                    .eq("key", "G" + nr)
                    .query();
            if(item.size() > 0)
                return item.get(0).getDefinition();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }


    public String getStrongs(String... nr) {
        List<String> list = new ArrayList<>();
        String strongs = "";
        try {
            for(String n : nr) {
                List<Strongs> item = getHelper().getStrongsDao().queryBuilder()
                        .where()
                        .eq("nr", n)
                        .query();
                if(item.size() > 0)
                    list.add(item.get(0).getText());
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        if(list.size() > 1) {
            for (String s : list) {
                strongs += s + "\n\r";
            }
        } else if(list.size() == 1) {
            strongs = list.get(0);
        } else {
            strongs = "No Entry Found";
        }

        return strongs;
    }
}
