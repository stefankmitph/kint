package stefankmitph.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by KumpitschS on 16.09.2015.
 */
@DatabaseTable(tableName = "books")
public class Book {
    @DatabaseField
    private String name;

    @DatabaseField
    private int nr;

    public Book() {}


    public String getName() {
        return name;
    }

    public int getNr() {
        return nr;
    }
}
