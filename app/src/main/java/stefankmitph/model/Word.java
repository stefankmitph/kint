package stefankmitph.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by KumpitschS on 14.04.2015.
 */
@DatabaseTable(tableName = "content")
public class Word {

    @DatabaseField
    private String book;

    @DatabaseField
    private int chapter;

    @DatabaseField
    private int verse;

    @DatabaseField
    private int wordnr;

    @DatabaseField
    private String word;

    @DatabaseField
    private String strongs;

    @DatabaseField
    private String lemma;

    @DatabaseField
    private String translit;

    @DatabaseField
    private String concordance;

    public Word() {}

    public String getLemma() {
        return lemma;
    }

    public String getWord() {
        return word;
    }

    public String getStrongs() {
        return strongs;
    }

    public int getWordnr() {
        return wordnr;
    }

    public String getTranslit() {
        if(translit != null)
            return translit;
        return "";
    }

    public int getVerse() {
        return verse;
    }

    public String getConcordance() {
        return concordance;
    }

    public int getChapter() {
        return chapter;
    }

    public String getBook() {
        return book;
    }
}
